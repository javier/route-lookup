#ifndef _MY_ROUTE_LOOKUP_H_
#define _MY_ROUTE_LOOKUP_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

short int error;		

unsigned short *tbl_24, *tbl_long;

int *processedPackets;
double *totalTableAccesses, *totalPacketProcessingTime;

struct timeval start, end;

static unsigned long const tbl_24_capacity = 16777216;

/********************************************************************
 *  Creates and populates TBL24 and TBLlong
 ********************************************************************/
void initializeFIB(void);
/********************************************************************
 *  Reads the IP address, calls select_interface and compute_measures
 ********************************************************************/
void compute_routes(void);
/********************************************************************
 *  Select the table and calculates the output interface
 ********************************************************************/
void select_interface(uint32_t *ip, unsigned char *table, unsigned short *interface);
/********************************************************************
 *  Prints each processed interface and measures time and accesses
 ********************************************************************/
void compute_measures(uint32_t *ip, unsigned short *interface,
                      struct timeval start, struct timeval end, 
                      unsigned char *number_of_tables, double *searching_time);

#endif