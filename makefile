SRC = my_route_lookup.c my_route_lookup.h io.c io.h
CFLAGS = -Wall -Wextra -O2 -march=athlon64 -pipe

all: my_route_lookup

my_route_lookup: my_route_lookup.c my_route_lookup.h io.c io.h
	gcc $(CFLAGS) $(SRC) -o my_route_lookup -lm

.PHONY: clean

clean:
	rm -f my_route_lookup

