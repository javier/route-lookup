#include "io.h"
#include "my_route_lookup.h"

int main(int argc, char *argv[])
{
    if (argc != 3) printf("It needs two parameters: ./my_route_lookup RIB InputPacketFile\n");
    else
    {
        if ((error = initializeIO(argv[1], argv[2])) != 0)
        {   //Generally the file is not found
            printIOExplanationError(error);
            return -1;
        }

    processedPackets  = calloc(1,sizeof(int));
    totalTableAccesses  = calloc(1,sizeof(double));
    totalPacketProcessingTime  = calloc(1,sizeof(double));

    initializeFIB();
    compute_routes();

    double averageTableAccesses = (*totalTableAccesses / *processedPackets),
           averagePacketProcessingTime = (*totalPacketProcessingTime / *processedPackets);

    printSummary(*processedPackets, averageTableAccesses, averagePacketProcessingTime);

    freeIO();free(tbl_24);free(tbl_long);
    free(processedPackets);free(totalTableAccesses);free(totalPacketProcessingTime);
}
return 0;
}

void initializeFIB()
{
    tbl_24 = calloc(tbl_24_capacity,sizeof(unsigned short));

    uint32_t *prefix = calloc(1,sizeof(uint32_t));
    unsigned short *prefix_length = calloc(1,sizeof(unsigned short)),
                   *out_interface = calloc(1,sizeof(unsigned short)),
                    expansion_index = 0;
    
    while((error = readFIBLine(prefix, prefix_length, out_interface)) == 0)
    {
        unsigned int hosts_masked, entry;
        /*"If X is less than or equal to 24 bits long, it need only be 
        stored in TBL24. The first bit of the entry is set to zero 
        to indicate that the remaining 15 bits designate the next-hop."*/
        if (*prefix_length <= 24)
        {
            hosts_masked = pow(2,24 - *prefix_length);
            
            for(entry = 0; entry < hosts_masked; entry++)
                tbl_24[(*prefix >> 8) + entry] = *out_interface;
        }
        else
        {
            hosts_masked = pow(2,32 - *prefix_length);
            //"If the first bit equals zero, then the remaining 15 bits describe the next hop"
            if (tbl_24[*prefix >> 8] >> 15 == 0)
            {
                tbl_long = realloc(tbl_long, 256*(expansion_index + 1)*2);
                /*"Each 24-bit prefix that has at least one route longer
                than 24 bits is allocated 2^8 = 256 entries in TBLlong"*/
                for(entry = 0; entry <= 255; entry++)
                    tbl_long[expansion_index*256 + entry] = tbl_24[*prefix >> 8];
                //"in TBL24, we insert a one followed by an index"
                tbl_24[*prefix >> 8] = 0x8000 | expansion_index;
                //"In the second table, we allocate entries starting with memory location expansion_index×256"
                for(entry = (*prefix & 0xFF); entry < hosts_masked + (*prefix & 0xFF); entry++)
                    tbl_long[expansion_index*256 + entry] = *out_interface;

                expansion_index++;
            }/*"Otherwise, multiply the remaining 15 bits by 256, add the product to the last 8
               bits of the original destination address (achieved by shifting and concatenation), 
               and use this value as a direct index into TBLlong"*/
            else for(entry = (*prefix & 0xFF); entry < hosts_masked + (*prefix & 0xFF); entry++)
                     tbl_long[(tbl_24[*prefix >> 8] & 0x7FFF)*256 + entry] = *out_interface;
        }
    }
    free(prefix);free(prefix_length);free(out_interface);
}

void select_interface(uint32_t *ip, unsigned char *table, unsigned short *interface)
{   //If the bit is 0, it means we don't need to see at table 2
    if ((*interface = tbl_24[*ip >> 8]) >> 15 == 0) *table = 1;
    else 
    {
        *table = 2; 
        *interface = tbl_long[(*interface & 0x7FFF)*256 + (*ip & 0x000000FF)];
    }
}

void compute_routes()
{
    uint32_t *ip = calloc(1,sizeof(uint32_t));
    unsigned short *interface = calloc(1,sizeof(unsigned short));
    unsigned char *table = calloc(1,sizeof(unsigned char));
    double *searching_time = calloc(1,sizeof(double));

    while((error = readInputPacketFileLine(ip)) == 0)
    {
        gettimeofday(&start, NULL);
        select_interface(ip, table, interface);
        gettimeofday(&end, NULL);

        compute_measures(ip, interface, start, end, table, searching_time);
    }

    free(ip);free(table);free(interface);free(searching_time);
}

void compute_measures(uint32_t *ip, unsigned short *interface,
                      struct timeval start, struct timeval end,
                      unsigned char *table, double *searching_time)
{
    printOutputLine(*ip, *interface, &start, &end, searching_time, *table);

    (*processedPackets)++;
    *totalTableAccesses  = *totalTableAccesses + *table;
    *totalPacketProcessingTime  = *totalPacketProcessingTime + *searching_time;
}
