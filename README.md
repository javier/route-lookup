## Descripción del algoritmo y las mejoras implementadas

Dos tablas, TBL24 y TBLlong se crean para realizar el route lookup.

TBL24 contiene 2^24 entradas. Se inician las tablas de forma que

- Si el prefijo es menor de 24 bits, el primer bit es 0 y los
15 siguientes muestran el siguiente salto.
	- Copiamos la interfaz de salida 2^(24-longitud_prefijo) veces
- Si el prefijo es mayor de 24 bits:
	- Si el bit 16 es un 0, entonces se expande la segunda tabla en 256 posiciones
	y se marca el bit 16 de TBL24 con un '1'.
	- Si es un 1, se escribe la interfaz en TBLlong.

Para buscar la interfaz, se mira el bit 16 de TBL24.

- Si es 0, significa que los datos están en la propia TBL24.
- Si es 1, significa que la TBL24 sirve de índice para obtener el contenido de TBLlong,
que es la que tiene la información de la interfaz.

Los cálculos de tiempo están implementados en IO.c.

## Condiciones bajo las que sostiene que su algoritmo tiene mejores prestaciones que
la búsqueda lineal en tablas.

En términos de acceso a tablas, SIEMPRE será mejor, ya que la búsqueda lineal require
al menos dos accesos, mientras que este multibit-trie siempre hará un número medio de
entre uno y dos accesos.

En términos de memoria RAM, este es un algoritmo costoso, pero se han hecho algunas
optimizaciones en el código y se ha reducido bastante su consumo.

A la hora de procesar los paquetes, es indudablemente más rápido, ya que procesa los
paquetes una media de dos órdenes de magnitud menos que el algoritmo de búsqueda lineal.

## Explique si el algoritmo es escalable a IPv6.

Citando al artículo, "IPv6 is still some way off", es decir, el consumo de memoria
que necesitaría esta versión de IP es muy alto, por lo que en principio no es viable.

Tal vez utilizando algunas mejoras como Subrange-Update, que se propone en el artículo,
sería posible la implementación, pero desde luego no está reflejado en el código proporcionado.
